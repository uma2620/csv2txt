"use strict";
exports.__esModule = true;
var converter = require("./controllers/csvtotxt");
var cron = require("node-cron");
var http = require("http");
cron.schedule("* * * * *", function () {
    console.log("converting every minute");
    converter.Tojson();
}, null);
http.createServer(function (req, res) {
    res.write('Converting csv to txt!'); //write a response
    res.end(); //end the response
}).listen(3000, function () {
    console.log("server start at port 3000"); //the server object listens on port 3000
});
