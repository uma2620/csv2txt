
import * as cj from 'csvjson';
import * as fs from 'fs';
var csvFilePath: String = './files/sample.csv';
var txtFilePath: String = './files/output.txt';


export const Tojson: any = () => {

    var options: any = {
        delimiter: ',', // optional
        quote: '"' // optional
    };
    var file_data: any = fs.readFileSync(csvFilePath, { encoding: 'utf8' });
    var result: any[] = cj.toObject(file_data, options);
    //console.log(result);
    var jsonData:String;
    jsonData = JSON.stringify(result, null, 1);
    //console.log("hi");
    //console.log(data);
    fs.writeFile(txtFilePath, jsonData, function (result, err) {
        if (err) {
            console.log(err);
        }
        else {
            console.log("succesfully converted");
        }
    });


}