"use strict";
exports.__esModule = true;
var cj = require("csvjson");
var fs = require("fs");
var csvFilePath = './files/sample.csv';
var txtFilePath = './files/output.txt';
exports.Tojson = function () {
    var options = {
        delimiter: ',',
        quote: '"' // optional
    };
    var file_data = fs.readFileSync(csvFilePath, { encoding: 'utf8' });
    var result = cj.toObject(file_data, options);
    //console.log(result);
    var jsonData;
    jsonData = JSON.stringify(result, null, 1);
    //console.log("hi");
    //console.log(data);
    fs.writeFile(txtFilePath, jsonData, function (result, err) {
        if (err) {
            console.log(err);
        }
        else {
            console.log("succesfully converted");
        }
    });
};
